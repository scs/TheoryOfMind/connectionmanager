"""
    The ConnectionManager is designed to abstract communication via different 
    middlewares (currently tcp, zmq and ipaaca) away from the rest of a program.

    The core idea is that you define "connections", either "outgoing" (i.e. "clients")
    or "incoming" (i.e. "servers"). Both types can send and receive messages, but are
    making different assumptions about the other side of the connection.
    The type of connection needs to be defined when creating
    them. For ZMQ and TCP this is required as there needs to be a server binding a socket and
    a client connecting to the socket.
    This also means that incoming connections could have multiple other ends.
    For ipaaca, things are a little bit different, as there we have "categories"/"channels".

    In all cases you define a connection, by passing it middleware specific parameters as
    well as a handler function, which will be called with the *json* formatted message that
    was received. 
    In order to send messages across the connection, there is a "send" method which will
    send data, depending on the type of connection:
    - outgoing zmq/tcp connection: Send only to the connected "server" part
    - incoming zmq/tcp connection: Send to all connected clients (unless specified otherwise)
    - ipaaca: Publish the message on the defined channel
"""

MIDDLEWARES = {prot: True for prot in ("tcp", "zmq", "ipaaca")}

try:
    import ipaaca
except (ImportError, SyntaxError):
    MIDDLEWARES["ipaaca"] = False
                  
try:
    import zmq
except (ImportError):
    MIDDLEWARES["zmq"] = False

try:
    import socketserver 
except:
    import SocketServer as socketserver

try:
    import queue
except ImportError:
    import Queue as queue

import json, logging, time, threading, socket

USE_ASYNCIO = False

logger = logging.getLogger(__name__)

sleeper = time.sleep

SLEEP_DURATION = 0.01 # seconds

POLL_TIME = 10 # ms

def get_params(middleware, _type = None):

    zmq_client_params = {
                        "connection_identifier": "",
                        "type": "outgoing",
                        "servername": "localhost",
                        "port":"",
                        "protocol": "tcp",
                        "client_ident": ""
                    }

    zmq_server_params = {
                            "connection_identifier": "",
                            "type": "incoming", #"server" would also be ok
                            "servername": "localhost",
                            "port":"",
                            "protocol": "tcp" #only required for zmq
                        }

    tcp_client_params = {
                        "connection_identifier": "",
                        "type": "outgoing",
                        "servername": "localhost",
                        "port":"",
                    }

    tcp_server_params = {
                            "connection_identifier": "",
                            "type": "incoming", #"server" would also be ok
                            "servername": "localhost",
                            "port":"",
                        }

    ipaaca_params = {
        "identifier": "ipaaca_connection",
        "listener_categories": ["example"],
        "publish_category": "testcategory"
    }

    if middleware == "zmq":
        if _type in ("incoming", "server"):
            return zmq_server_params
        if _type in ("outgoing", "client"):
            return zmq_client_params 
    if middleware == "tcp":
        if _type in ("incoming", "server"):
            return tcp_server_params
        if _type in ("outgoing", "client"):
            return tcp_client_params 
    if middleware == "ipaaca":
        return ipaaca_params

    raise ValueError("No parameters for {} of type {}".format(middleware, _type))

class Connection(object):

    def __init__(self, handler, params):
        """
            All connections need to be given a handler function
            which will be called with the chunk of data being
            received by the connection.
        """
        raise NotImplementedError("Needs to be implemented by subclass")
    
    def send(self, msg, **kwargs):
        """
            Syncronous method to send the given message over
            the connection, provided that the connection was
            established previously.
        """
        raise NotImplementedError("Needs to be implemented by subclass")

    def close(self):
        raise NotImplementedError("Needs to be implemented by subclass")

    @staticmethod
    def create_connection(identifier, middleware, handler, params):
        if middleware == "ipaaca":
            return IpaacaConnection(identifier, handler, params)
        elif middleware == "tcp":
            return TCPConnection(identifier, handler, params)
        elif middleware == "zmq":
            return ZMQConnection(identifier, handler, params)
        else:
            logger.error("Unknown protocol: {}".format(middleware))
            
    @staticmethod
    def send_message(middleware, params, message):
        ident = "message"
        handler = None
        if middleware == "ipaaca":
            con = IpaacaConnection(ident, handler, params)
        elif middleware == "tcp":
            con = TCPConnection(ident, handler, params)
        elif middleware == "zmq":
            con = ZMQConnection(ident, handler, params)
        else:
            logger.error("Unknown protocol: {}".format(middleware))
        if con:
            con.send(message)
            sleeper(0.1)
            con.close()

#region ZMQ 
class ZMQServer(object):
    """
        A ZMQ "Server" using the ROUTER socket.
    """
    def __init__(self, context, handler, servername, port, protocol="tcp"):
        
        if servername == "localhost":
            # Workaround for zmq "operation not supported" error
            servername = "127.0.0.1"
        self.servername = servername
        self.clients = []
        self.port = port
        self.handler = handler
        self.context = context
        self.socket = self.context.socket(zmq.ROUTER)
        request_port = "{}://{}:{}".format(protocol, servername, port)
        logger.info("Setting up ZMQ server on {}".format(request_port))
        self.socket.setsockopt(zmq.LINGER, 0)
        self.socket.set_hwm(2)
        self.socket.bind(request_port)
        self.server_thread = threading.Thread(target=self.serve_forever)
        self.server_thread.daemon = True
        self.running = True
        self.out_queue = queue.Queue()
        self.server_thread.start()

    def serve_forever(self):

        poller = zmq.Poller()
        poller.register(self.socket, zmq.POLLIN)
               
        while self.running:
            sleeper(SLEEP_DURATION)
            evts = poller.poll(POLL_TIME)
            # while evts:
            #     ident, msg  = self.socket.recv_multipart()
            #     evts = poller.poll(POLL_TIME)

            if evts:  
                ident, msg  = self.socket.recv_multipart()
                logger.debug("received: {} from {}".format(msg, ident))
                if not ident in self.clients:
                    self.clients.append(ident)
                if msg:
                    # message = self.socket.recv_json()
                    # self.socket.send_json("acknowledged")
                    decoded_msg = msg.decode('unicode_escape').strip('"')
                    message = json.loads(decoded_msg)
                    # message = json.loads(msg.strip('"'))
                    logger.debug("Server received data: {}, loaded type: {}".format(message, type(message)))
                    message["_connection"] = "zmq:{}:{}".format(self.servername,self.port)
                    self.handler(message)
            item = None
            while not self.out_queue.empty():
                try:
                    item = self.out_queue.get(timeout=0.01)
                except queue.Empty:
                    logger.debug("Queue was empty")
                    break
                # Only send the newest item to the socket.
                if item:
                    client, msg = item 
                    if client and client in self.clients:
                        self.socket.send_multipart([client, json.dumps(msg).encode('utf-8')])
                    else:
                        for client in self.clients:
                            self.socket.send_multipart([client, json.dumps(msg).encode('utf-8')])


    def send(self, msg, client=None, **kwargs):
        logger.debug("sending message {} to {} (all clients)".format(msg, self.clients))
        self.out_queue.put((client, msg))
        # if client and client in self.clients:
        #     self.socket.send_multipart([client, json.dumps(msg).encode('utf-8')])
        # else:
        #     for client in self.clients:
        #         self.socket.send_multipart([client, json.dumps(msg).encode('utf-8')])

    def shutdown(self):
        self.running = False
        self.socket.close()

    def __del__(self):
        self.shutdown()

     
class ZMQConnection(Connection):
    """
        A ZMQ "client" connection using the DEALER socket to connect
        to another ROUTER.
    """
    context = zmq.Context.instance()
    
    def __init__(self, identifier, handler, params):
        self.identifier = identifier
        self.params = params
        self.handler = handler
        self.running = True
        self.server = None
        if params.get("type", "client") in ("server", "incoming"):
            servername = params.get("servername", "localhost") #socket.gethostname() if we need to allow different machines!
            port = params.get("port", "6565")
            self.server = ZMQServer(self.context, handler, servername, port, params.get("protocol", "tcp"))

        elif params.get("type", "client") in ("client", "outgoing"):
            logger.info("Setting up ZMQ client")
            self.handler = handler
            self.running = True

            protocol = params.get("protocol", "tcp")
            port = params.get("port", "6565")
            servername = params.get("servername", "localhost")
            # if servername == "localhost":
            #     # Workaround for zmq "operation not supported" error
            #     servername = "127.0.0.1"

            self.port = port 
            self.servername = servername
            self.out_queue = queue.Queue()
            ident = params.get("client_ident", identifier)
            self.dealer = self.context.socket(zmq.DEALER)
            self.dealer.setsockopt(zmq.IDENTITY, bytes(ident, encoding="utf-8"))
            self.dealer.setsockopt(zmq.LINGER, 0)
            self.dealer.set_hwm(2)
            request_port = "{}://{}:{}".format(protocol, servername, port)
            logger.info("Connection to ZMQ server {}".format(request_port))
            self.dealer.connect(request_port)
            self.dealer.send_json(json.dumps({"type":"hello"}))
            self.receiver_thread = threading.Thread(target=self.receive)
            self.receiver_thread.daemon = True
            logger.debug("before starting receiver thread")
            self.receiver_thread.start()
            logger.debug("after starting receiver thread")

    def receive(self):
        poller = zmq.Poller()
        poller.register(self.dealer, zmq.POLLIN)
        while self.running:
            evts = poller.poll(POLL_TIME)
            # while evts:
            #     msg = self.dealer.recv()
            #     evts = poller.poll(POLL_TIME)

            if evts:
                msg = self.dealer.recv()
                logger.debug("received data: {}, loaded type: {}".format(msg, type(msg)))
                message = json.loads(msg)
                    # message = json.loads(msg.strip('"'))
                    # logger.info("received data: {}, loaded type: {}".format(message, type(message)))
                message["_connection"] = "zmq:{}:{}".format(self.servername,self.port)
                if self.handler:
                    self.handler(message)
                else:
                    logging.error("Received reply for message: {}".format(msg))

            item = None
            while not self.out_queue.empty():
                try:
                    item = self.out_queue.get(timeout=0.01)
                except queue.Empty:
                    logger.debug("Queue was empty")
                    break
                # Only send the newest item to the socket.
                if item:
                    self.dealer.send_json(item)   
            
            sleeper(SLEEP_DURATION)
        self.dealer.close()

    def close(self):
        logger.info("closing connection {}".format(self.identifier))
        self.running = False
        if self.server:
            self.server.shutdown()
        else:
            # self.dealer.close()
            logger.debug("dealer closed: {}".format(self.dealer.closed))
        
        logger.debug("before term")
        # self.context.destroy(linger=5)
        logger.debug("after destroy")

    def send(self, msg, **kwargs):
        logger.debug("sending: {}".format(msg))
        if self.server:
            self.server.send(msg, **kwargs)
        else:
            logger.debug("client send")
            self.out_queue.put(msg)
            # self.dealer.send_json(msg)    

#endregion

#region Ipaaca
class IpaacaConnection(Connection):
    
    def __init__(self, identifier, handler, params):
        self.identifier = identifier
        self.params = params
        self.handler = handler
        self.output_category = None
        # Always create an output buffer
        self.output_buffer = ipaaca.OutputBuffer(params.get("identifier", "default_component"))
        if "listener_categories" in params:
            self.input_buffer = ipaaca.InputBuffer(params.get("identifier", "default_component"), 
                                                    category_interests=params["listener_categories"])
            self.input_buffer.register_handler(self.handle_iu)
        else:
            logger.warn("Ipaaca connection without listener categories {}".format(params))
        if "publish_category" in params:
            self.output_category = params["publish_category"]
        else:
            logger.warn("Ipaaca connection without a specified output category, " \
                "you MUST specifiy the desired category when sending messages yourself {}".format(params))
        
    def send(self, msg, category=None, **kwargs):
        if not category:
            category = self.output_category
        iu = ipaaca.IU(category)
        iu.payload = {"iu_payload": msg}
        logger.debug("Sending {} via ipaaca".format(msg))
        self.output_buffer.add(iu)

    def handle_iu(self, iu, event_type, local):
        message = iu.payload["iu_payload"]
        message["_connection"] = "ipaaca:"+iu.category
        self.handler(message)

#endregion 
        
#region TCP
class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    
    def __init__(self, server_address, RequestHandlerClass, bind_and_activate=True, 
                    incoming_queue=None, outgoing_queue=None):
        self.incoming_queue = incoming_queue
        self.outgoing_queue = outgoing_queue
        socketserver.TCPServer.allow_reuse_address = True
        socketserver.TCPServer.__init__(self, server_address, RequestHandlerClass,
                           bind_and_activate=bind_and_activate)

class TCPStreamHandler(socketserver.StreamRequestHandler):

    """
        The TCP connection handler. This will get the queue objects
        from the server instance for each new connection (there should only be one
        usually).

        The incoming_queue is for messages received over the TCP connection, i.e
        incoming into the current process.

        The outgoing_queue is for messages that the current process wants to send
        to the other process.
    """

    def __init__(self, request, client_address, server):
        self.incoming_queue = server.incoming_queue
        self.outgoing_queue = server.outgoing_queue
        socketserver.StreamRequestHandler.__init__(self, request, client_address, server)

    def setup(self):
        super(TCPStreamHandler, self).setup()
        logger.debug("New connection from : {}".format(self.client_address))
        self.connected = True

    def handle_read(self):
        while self.connected:
            chunk = None
            try:
                chunk = self.rfile.readline()
            except Exception as e:
                logger.exception("Socket read exception: ")
            if not chunk:
                logger.info("Connection closed by client({})".format(self.client_address))
                self.connected = False
            else:
                try:
                    js = json.loads(chunk.strip())
                    self.incoming_queue.put(js)
                except json.decoder.JSONDecodeError:
                    logger.exception("JSON Decode error")
            sleeper(SLEEP_DURATION)
        
    def handle(self):
        """
            The main handler method for the connection. 
            This will continuously check for new incoming messages from the
            connection and pass them on to the incoming_queue to be processed by the
            connection handler.

            It will also check if there is a new message in the outgoing_queue and
            send any messages there to the client.
        """
        listen_thread = threading.Thread(target=self.handle_read)
        listen_thread.daemon = True
        listen_thread.start()

        while self.connected:
            # Send all messages waiting to be send out
            while not self.outgoing_queue.empty():
                try:
                    item = self.outgoing_queue.get(timeout=0.01)
                except queue.Empty:
                    logger.debug("Queue was empty")
                    break
                if item:
                    self.wfile.write(bytes(item + "\n", "utf-8"))
            sleeper(SLEEP_DURATION)

class TCPConnection(Connection):
    
    def __init__(self, identifier, handler, params):
        self.identifier = identifier
        self.params = params
        
        self.handler = handler

        self.incoming_queue = queue.Queue()
        self.outgoing_queue = queue.Queue()

        self.server = None 
        self.server_thread = None 
        self.client_thread = None
        self.running = True

        self.servername = params.get("servername", "localhost") #socket.gethostname() if we need to allow different machines!
        self.port = params.get("port", "6565")

        if params.get("type", "server") in ("server", "incoming"):
            logger.info("Setting up TCP server")
            self.server = ThreadedTCPServer((self.servername, self.port), TCPStreamHandler, 
                        incoming_queue=self.incoming_queue, outgoing_queue=self.outgoing_queue)
            
            self.server_thread = threading.Thread(target=self.server.serve_forever)
            # Exit the server thread when the main thread terminates
            self.server_thread.daemon = True
            self.server_thread.start()
            logger.info("Finished setting up tcp server")
        elif params.get("type", "server") in ("client", "outgoing"):
            logger.info("Setting up TCP client")
            self.client_thread = threading.Thread(target=self.listen_forever) 
            self.client_thread.daemon = True
            self.client_thread.start()

        self.handler_thread = threading.Thread(target=self.handle_messages)
        self.handler_thread.daemon = True 
        self.handler_thread.start()

    def handle_messages(self):
        """
            Threaded function for handling incoming messages and executing the
            handler function with the received messages. This is a seperate 
            function in order to not block receiving messages while executing
            the handler.
        """
        while self.running:
            if not self.incoming_queue.empty():
                try:
                    item = self.incoming_queue.get(timeout=0.01)
                except queue.Empty:
                    logger.debug("Queue was empty")
                if item:
                    message = item 
                    message["_connection"] = "tcp:{}:{}".format(self.servername, self.port)
                    self.handler(item)
            sleeper(SLEEP_DURATION)

    def handle_read(self):
         while self.running:
            data = b""
            msg_complete = False
            while not msg_complete:
                chunk = self.sock.recv(1024)
                if not chunk:
                    logger.info("Connection closed by server")
                    break
                else:
                    # Check for terminating symbol
                    if chunk.endswith(b"\n"):
                        msg_complete = True
                    data += chunk 
            if not data:
                logger.info("Connection closed by server")
                break
            else:
                try:
                    js = json.loads(data.strip())
                    self.incoming_queue.put(js)
                except json.decoder.JSONDecodeError:
                    logger.exception("JSON Decode error")
            sleeper(SLEEP_DURATION)

    def listen_forever(self):
        logger.debug("Setting up client socket")
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((self.servername, self.port))

        listen_thread = threading.Thread(target=self.handle_read)
        listen_thread.daemon = True
        listen_thread.start()
        while self.running:
            # Write all current elements in the queue. 
            item = None
            while not self.outgoing_queue.empty():
                try:
                    item = self.outgoing_queue.get(timeout=0.01)
                except queue.Empty:
                    logger.debug("Queue was empty")
                    break
                # Only send the newest item to the socket.
                if item:
                    self.sock.sendall(bytes(item + "\n", "utf-8"))

            sleeper(SLEEP_DURATION)

        self.sock.close()


    def send(self, msg, **kwargs):
        # Place the msg into the outgoing queue. The TCPHandler will 
        # send the message out as quickly as possible.
        logger.debug("sending message {} to client (TCP)".format(msg))
        self.outgoing_queue.put(msg)

    def close(self):
        self.running = False
        if self.server:
            self.server.shutdown()
            self.server.server_close()

    def __del__(self):
        self.close()
#endregion
        
class ConnectionManager(object):
    """
        The actual connection Manager that abstracts away as much as possible
        from the actual underlying middlewares and tries to offer a humogenous 
        interface to handle connections.
    """
    
    def __init__(self):
        self.connections = {}

    def open_connection(self, middleware, params, callback, identifier=None):
        if not MIDDLEWARES[middleware]:
            raise ValueError("Requested middleware ({}) is not available, you may need to install it first!".format(middleware))
        logger.debug("opening connection via {} with params {}".format(middleware, params))
        if identifier is None:
            identifier = params.get("connection_identifier", "con{}".format(len(self.connections)))
        if not identifier in self.connections:
            self.connections[identifier] = Connection.create_connection(identifier, middleware, callback, params)
        else:
            logger.info("There was already a connection called {}".format(identifier))

    def close_connection(self, identifier):
        if identifier in self.connections:
            # May be double, but I find that Python occasionally does not call
            # __del__ correctly so make sure it is closed properly.
            self.connections[identifier].close()
            del self.connections[identifier]

    def disconnect(self):
        """
            Closes all current connections.
        """
        for ident, con in self.connections.items():
            con.close()
        self.connections.clear()

    def send_message(self, middleware, params, msg):
        logger.debug("before sending message")
        Connection.send_message(middleware, params, msg)
        logger.debug("After sending message")

    def notify(self, con_identifier, msg, client_identifier=None):
        logger.debug("notify {} (current connections: {})".format(con_identifier, self.connections))
        if con_identifier in self.connections:
            self.connections[con_identifier].send(msg, client=client_identifier)

    def send(self, msg):
        """
            Send a message across all connections
        """
        for con in self.connections:
            self.connections[con].send(msg)
        
    def clear(self):
        for con in list(self.connections.keys()):
            self.connections[con].close()
            del self.connections[con]

    def __del__(self):
        logger.info("deconstructing connectionManager")
        self.clear()
