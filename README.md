# connectionManager

Maintained by Jan Pöppel (jpoeppel@techfak.uni-bielefeld.de)

A small abstraction layer for different middlewares (tcp, zmq and ipaaca) providing a uniform interface.
It makes certain assumptions regarding the type of communication in order to
take away a lot of the boilerplate code required otherwise. 

One of these assumptions is that only json-formatted data is being send across
these connections.

The `asyncio` branch is the most up-to-date and should be used unless you run into serious problems with it.
However, ipaaca has not been converted to using asyncio even in that branch so far.

*So far this tool is still under development and feedback is strongly welcome if you find yourself using it!*


## Hardware Dependencies

None 

## Supported operating systems

Linux, Windows, Mac

## Build dependencies

Python 3

## Run dependencies

* Python >= 3 (Python 2 does also work, but no conda packages will be created for Python 2)
  * ipaaca (optional if you want to use ipaaca)
  * zmq (optional if you want to use zmq)

## Installation

You can install the module **globally** (or within your current conda environment)
using ```python setup.py install```. 

If the SCS group is interested in this, there may also be a conda-package for
this module soon.

## Usage

The module is intended to be used within other projects. It currently only contains
a single module with the key class being the ```ConnectionManager``` which provides
the following API:

### add_connection(middleware, params, callback, ident=None)

This function allows you to add a connection using the specified middleware.
Each middleware will require different parameters in order to correctly setup
a connection. There will likely be sensible default parameters included in the
connection manager in the future.

The callback argument is a function handler, which will be called with the
each incoming message as argument.

### close_connection(identifier)

This function will close the specified connection if it is currently active.
Closing a connection may also involve notifying the other end about the imminent
closure, depending on the used middleware (e.g. zmq will notify explicitly).

### notify(con_identifier, msg)

Will send the given message via the specified connection. This will only send the
message across the one connection. There may still be multiple "listeners" for 
that connection as ipaaca broadcasts messages across channels or a tcp/zmq server
may have multiple clients.

### send(msg)

This function will send the given message across all currently active connections.