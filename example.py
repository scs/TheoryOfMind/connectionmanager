import logging 
logging.basicConfig(level=logging.DEBUG)

from connectionManager import ConnectionManager

import json, time

# Params for zmq or tcp servers
socket_server_params = {
    "identifier": "socket_server",
    "type": "incoming", #"server" would also be ok
    "servername": "localhost",
    "port": 6642,
    "protocol": "tcp" #only required for zmq
}

# Params for zmq or tcp clients
socket_client_params = {
    # "identifier": "socket_client",
    "type": "outgoing", #"client" would also be ok
    "servername": "localhost",
    "port": 6666,
    "protocol": "tcp", #only required for zmq
    # "client_ident": "testclient"  #only required for zmq
}

socket_client_params2 = dict(socket_client_params)
socket_client_params2["port"] = 6642
socket_client_params2["client_ident"] = "testclient2"

# Params for ipaaca 
ipaaca_params = {
    "identifier": "ipaaca_connection",
    "listener_categories": ["example"],
    "publish_category": "testcategory" #defaults to default if not given
}

msg = {"type": "testmessage", "payload": "dummy"}

def handler(msg):
    print("handling message: ", msg)

if __name__ == "__main__":
    import sys 
    if len(sys.argv) > 1:
        con_man_client = ConnectionManager()
        con_man_client.open_connection("zmq", socket_client_params2, handler)
        con_man2 = ConnectionManager()
        # con_man2.open_connection("zmq", socket_client_params, handler)
        # con_man_client.open_connection("tcp", socket_client_params, handler)
        # ipaaca_params["listener_categories"] = "testcategory"
        # ipaaca_params["publish_category"] = "example"
        # con_man_client.open_connection("ipaaca", ipaaca_params, handler)
    else:
        con_man_server = ConnectionManager()
        con_man_server.open_connection("zmq", socket_server_params, handler)
        # con_man_server.open_connection("tcp", socket_server_params, handler)
        # con_man_server.open_connection("ipaaca", ipaaca_params, handler)

    # if len(sys.argv) > 1:
    #     con_man_client.send(json.dumps(msg))
    #     time.sleep(0.1)
    #     con_man_client.clear()
    # else:
    i = 0
    # time.sleep(3)    
    try:
        while True:
        # for i in range(10):
            time.sleep(1)
            if len(sys.argv) > 1:
                print("should send")
                con_man_client.send(json.dumps(msg))
                i += 1
                if i == 3:
                    con_man_client.clear()
                    # break

                if i == 5:
                    con_man_client.open_connection("zmq", socket_client_params2, handler)
            
                # con_man2.send(json.dumps({"type":"con_man2"}))
                con_man2.send_message("zmq", {
                            "servername": "localhost",
                            "port": 6642,
                            "type": "client",
                            "protocol": "tcp", #only required for zmq
                        }, json.dumps({"type":"con_man2"}))
            # if len(sys.argv) == 1:
            #     con_man_server.send(json.dumps(msg))
    except KeyboardInterrupt:
        if len(sys.argv) > 1:
            con_man2.clear()
            con_man_client.clear()
        else:
            con_man_server.clear()
        
