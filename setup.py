from setuptools import setup, find_packages

NAME = "connectionManager"
AUTHOR = 'Jan Pöppel'
DESCRIPTION = 'A small package abstracting away different middlewares (tcp, zmq and ipaaca)'
URL = 'https://gitlab.ub.uni-bielefeld.de/jpoeppel/connectionManager'
EMAIL = "jpoeppel@techfak.uni-bielefeld.de"
VERSION = "1.0"

setup(
    name=NAME,
    version=VERSION,
    description=DESCRIPTION,
    author=AUTHOR,
    author_email=EMAIL,
    url=URL,
    # packages=find_packages(),
    py_modules=["connectionManager"],
    zip_safe=False,
    license='MIT'
)